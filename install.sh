###!/usr/bin/bash
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

rm -rf $HOME/.zsh
git clone --depth 1 https://github.com/zsh-users/zsh-autosuggestions $HOME/.zsh/plugins/zsh-autosuggestions
git clone --depth 1 https://github.com/zsh-users/zsh-syntax-highlighting $HOME/.zsh/plugins/zsh-syntax-highlighting
git clone --depth 1 https://github.com/marlonrichert/zsh-autocomplete $HOME/.zsh/plugins/zsh-autocomplete
git clone --depth 1 https://github.com/romkatv/powerlevel10k.git $HOME/.zsh/plugins/powerlevel10k
ln -sf $SCRIPT_DIR/zshrc $HOME/.zshrc

ln -sf $SCRIPT_DIR/p10k.zsh $HOME/.p10k.zsh
