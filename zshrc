# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

source "$HOME/.zsh/plugins/zsh-autocomplete/zsh-autocomplete.plugin.zsh"
source "$HOME/.zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.plugin.zsh"
source "$HOME/.zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.plugin.zsh"
source "$HOME/.zsh/plugins/powerlevel10k/powerlevel10k.zsh-theme"

# History options
HISTSIZE="10000"
SAVEHIST="10000"

HISTFILE="$HOME/.zsh_history"
mkdir -p "$(dirname "$HISTFILE")"

setopt HIST_FCNTL_LOCK
setopt HIST_IGNORE_DUPS
setopt HIST_IGNORE_SPACE
unsetopt HIST_EXPIRE_DUPS_FIRST
setopt SHARE_HISTORY
unsetopt EXTENDED_HISTORY
setopt autocd

zstyle -e ':autocomplete:*' list-lines 'reply=( $(( LINES / 5 )) )'

# Aliases
alias ..='cd ..'
alias ...='cd ../..'
alias cat='bat --style header --style rule --style snip --style changes --style header'
alias grep='grep --color=auto'
alias l.='exa -a | egrep '\''^\.'\'''
alias la='exa -a --color=always --group-directories-first --icons'
alias ll='exa -l --color=always --group-directories-first --icons'
alias ls='exa -al --color=always --group-directories-first --icons'
alias lt='exa -aT --color=always --group-directories-first --icons'
alias pdflatex='pdflatex -shell-escape'
alias psmem='ps auxf | sort -nr -k 4'
alias psmem10='ps auxf | sort -nr -k 4 | head -10'
alias ssh='ssh -AYC'
alias tarnow='tar -acvf'
alias untar='tar -zxvf'
alias wget='wget -c'
alias z='zellij'
alias za='zellij a'

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

# Created by `pipx` on 2024-06-19 19:06:52
export PATH="$PATH:/Users/boris/.local/bin"
